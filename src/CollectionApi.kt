val canBeInClub27: (Person) -> Boolean = { p: Person -> p.age <= 27 }

fun main() {
    val people = createPersons()
    val books = createBooks()

    println(people.any(canBeInClub27))
    println(people.count(canBeInClub27))
    println(people.find(canBeInClub27))
    println(people.groupBy { it.age })

    println("----------------------------------------")

    println(books.flatMap { it.authors }.toSet())

    val list = listOf(1, 2, 3, 4)
    println(list.filter { it % 2 == 0 })

    println(list.map { it * it })

}

private fun createBooks(): List<Book> {
    return listOf(
        Book("Thursday Next", listOf("Jasper Fforde")),
        Book("Mort", listOf("Terry Pratchett")),
        Book("Good Omens", listOf("Terry Pratchett", "Neil Gaiman"))
    )
}

private fun createPersons(): List<Person> {
    return listOf(
        Person("Alice", 29),
        Person("Bob", 31),
        Person("Ruslan", 33),
        Person("Ildar", 36),
        Person("Ilgiz", 22),
        Person("Ilnur", 22),
        Person("Ilshat", 23),
        Person("Insaf", 25)
    )
}

