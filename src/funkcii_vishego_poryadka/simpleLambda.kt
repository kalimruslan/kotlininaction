package funkcii_vishego_poryadka

import java.lang.StringBuilder

fun main() {
    twoAndThree { a, b -> a + b }
    twoAndThree { a, b -> a * b }
    println("ab1c".filter { it in 'a'..'z' })

    println(listOf(1, 2, 3, 4, 5, 6, 7, 8, 9, 10).joinToString())
    println(listOf(1, 2, 3, 4, 5, 6, 7, 8, 9, 10).joinToStringWithLambda())
}

fun twoAndThree(operation: (Int, Int) -> Int) {
    val result = operation(2, 3)
    println("The result is $result")
}

fun String.filter(predicate: (Char) -> Boolean): String {
    val sb = StringBuilder()
    for (index in 0 until length) {
        val element = get(index)
        if (predicate(element)) sb.append(element)
    }
    return sb.toString()
}

fun <T> Collection<T>.joinToString(
    separator: String = ", ",
    prefix: String = "",
    postfix: String = ""
): String {
    val result = StringBuilder(prefix)

    for ((index, element) in this.withIndex()) {
        if (index > 0) result.append(separator)
        result.append(element)
    }
    result.append(postfix)
    return result.toString()
}

fun <T> Collection<T>.joinToStringWithLambda(
    separator: String = ", ",
    prefix: String = "",
    postfix: String = "",
    transform: (T) -> String = { it.toString() }
): String {
    val result = StringBuilder(prefix)

    for ((index, element) in this.withIndex()) {
        if (index > 0) result.append(separator)
        result.append(transform(element))
    }
    result.append(postfix)
    return result.toString()
}
