package funkcii_vishego_poryadka

fun main() {
    // region first example
    val calculator = getShippingCostCalculator(Delivery.EXPEDITED)
    println("Shipping costs for EXPEDITED: ${calculator(Order(3))}")
    // endregion

    println("---------------------------------------------------------------")

    // region second example
    val contacts = createContacts()
    val contactListFilters = ContactListFilters()
    with(contactListFilters) {
        prefix = "Ru"
    }
    println("Found contact: ${contacts.filter(contactListFilters.getPredicate())}")
    // endregion

    println("---------------------------------------------------------------")

    // third example
    val log = createSiteVisiters()
    println("WINDOWS duration:  ${log.averageDurationFor(OS.WINDOWS)}")
    println("ANDROID duration:  ${log.averageDurationFor(OS.ANDROID)}")
    println("IOS duration:  ${log.averageDurationFor(OS.IOS)}")
    // endregion
}

//region first example
enum class Delivery { STANDARD, EXPEDITED }

class Order(val itemCount: Int)

fun getShippingCostCalculator(delivery: Delivery): (Order) -> Double {
    if (delivery == Delivery.EXPEDITED) {
        return { order -> 6 + 2.1 * order.itemCount }
    }

    return { order -> 1.2 * order.itemCount }
}
// endregion

// region second example
data class Person(
    val firstName: String,
    val lastName: String,
    val phoneNumber: String?
)

class ContactListFilters {
    var prefix: String = ""
    var onlyWithPhoneNumber: Boolean = false

    fun getPredicate(): (Person) -> Boolean {
        val startsWithPrefix = { p: Person -> p.firstName.startsWith(prefix) || p.lastName.startsWith(prefix) }
        if (!onlyWithPhoneNumber)
            return startsWithPrefix
        return { startsWithPrefix(it) && it.phoneNumber != null }
    }
}

private fun createContacts(): List<Person> {
    return listOf(
        Person("Alice", "Alicin", "283921748712"),
        Person("Bob", "Sinclair", null),
        Person("Ruslan", "Kalimullin", "89872708136")
    )
}
// endregion

// region third example
data class SiteVisit(
    val path: String,
    val duration: Double,
    val os: OS
)

enum class OS { WINDOWS, LINUX, MAC, IOS, ANDROID }

private fun createSiteVisiters(): List<SiteVisit> {
    return listOf(
        SiteVisit("/", 34.0, OS.WINDOWS),
        SiteVisit("/", 22.0, OS.MAC),
        SiteVisit("/login", 12.0, OS.WINDOWS),
        SiteVisit("/signup", 8.0, OS.IOS),
        SiteVisit("/register", 55.0, OS.ANDROID),
        SiteVisit("/", 16.3, OS.ANDROID)
    )
}

fun List<SiteVisit>.averageDurationFor(os: OS) = filter { it.os == os }.map(SiteVisit::duration).average()
// endregion

