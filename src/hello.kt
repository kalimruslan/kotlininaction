import java.lang.StringBuilder

fun main(){
    val people = listOf<Person>(Person("Alice", 29), Person("Bob", 31))
    println(people.maxBy { p:Person ->p.age })
    println(people.maxBy { it.age })
    findTheOldest(people)

    val sum = {x:Int, y:Int -> x + y}
    println(sum(1,2))

    println(alphabet())
}

fun alphabet(): String {

    /* val result = StringBuilder()
    for(letter in 'A'..'Z')
        result.append(letter)
    result.append("\nNow I know the alphabet!")
    return result.toString()*/

    val stringBuilder = StringBuilder()
    return with(stringBuilder){
        for(letter in 'A'..'Z')
            this.append(letter)
        append("\nNow I know the alphabet!")
        this.toString()
    }
}

fun findTheOldest(people: List<Person>){
    var maxAge = 0
    var theOldest: Person? = null
    for(person in people){
        if(person.age > maxAge){
            maxAge = person.age
            theOldest = person
        }
    }
    println(theOldest)
}